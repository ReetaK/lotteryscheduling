/* Lottery Edit -----------------------*/
#include "rand.h"
static int g_seed = 1234;

//Used to seed the generator.           
void rand_seed( int seed )   
{
        g_seed = seed;
}
int rand()
{
        g_seed = (214013*g_seed+2531011);
          return (g_seed>>16)&0x7FFF;
}
/* Lottery Edit -----------------------*/

